import axios from "axios";

class LuminosityHandler {
	private luminosity: number;
	private currentWorker: NodeJS.Timeout;
	constructor() {
		this.luminosity = 0;
	}

	getLuminosity() {
		return this.luminosity;
	}

	setLuminosity(newLuminosityLevel: number) {
		this.luminosity = newLuminosityLevel;
	}

	private sendToArduino() {
		// call API w/ param = this.luminosity
		// In the meantime for more clarity
		if (this.luminosity >= 0 && this.luminosity <= 99) {
			console.log("Luminosity level = ", this.luminosity, "%");
			axios({
				method: "get",
				url: `http://arduino/?power=${this.luminosity.toString().padStart(2, "0")}`,
			});
		} else {
			console.log("Luminoisty level out of range: ", this.luminosity, "%");
		}
	}

	private increaseLuminosity() {
		this.luminosity += 1;
		this.sendToArduino();
	}

	start() {
		console.log("Starting luminosity handler");
		this.currentWorker = setInterval(() => {
			this.increaseLuminosity();
			if (this.luminosity >= 99) {
				clearInterval(this.currentWorker);
			}
		}, 1000);
	}

	turnLightOff() {
		this.luminosity = 0;
		clearInterval(this.currentWorker);
		this.sendToArduino();
	}
}

export default LuminosityHandler;
