import schedule from "node-schedule";
import dayjs from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
import * as db from "./services/db";
import LuminosityHandler from "./LuminosityHandler";
dayjs.extend(localizedFormat);

class MinuteWorker {
	private static instance: MinuteWorker;
	private alarmIsActive: boolean;
	private luminosityHandler: LuminosityHandler;
	constructor() {
		this.alarmIsActive = false;
		this.luminosityHandler = new LuminosityHandler();
	}

	static getInstance() {
		if (!MinuteWorker.instance) {
			MinuteWorker.instance = new MinuteWorker();
		}
		return MinuteWorker.instance;
	}

	stopAlarm() {
		console.log("Stopping alarm");
		this.alarmIsActive = false;
		this.luminosityHandler.turnLightOff();
	}

	private async findAlarm() {
		if (!this.alarmIsActive) {
			const now = dayjs();
			const currentDay = now.format("ddd");
			const currentHour = now.format("H");
			const currentMinutes = now.format("m");
			const activeAlarms = await db.getCurrentActiveAlarms(
				currentDay,
				Number(currentHour),
				Number(currentMinutes),
			);
			if (activeAlarms && activeAlarms.length) {
				return activeAlarms[0];
			}
		}
		return null;
	}

	private launchAlarm() {
		console.log("Launching alarm");
		this.luminosityHandler.start();
		this.alarmIsActive = true;
	}

	private async checkAlarms() {
		console.log("Checking alarms every minute");
		if (!this.alarmIsActive) {
			const currentAlarm = await this.findAlarm();
			console.log("currentAlarm", currentAlarm);
			if (currentAlarm) {
				this.launchAlarm();
			}
		}
	}

	async start() {
		console.log("Starting minute worker");
		schedule.scheduleJob({ second: 5 }, () => {
			console.log("checking alarms");
			this.checkAlarms();
		});
	}
}

const minuteWorker = MinuteWorker.getInstance();

export default minuteWorker;
