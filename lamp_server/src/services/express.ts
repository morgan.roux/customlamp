import express, { Express, Request, Response } from "express";
import * as db from "./db";

const app: Express = express();
const port = 3001;

app.get("/", (req: Request, res: Response) => {
	res.send("Express + TypeScript Server");
});

app.get("/alarms", (req: Request, res: Response) => {
	const allAlarms = db.getAllAlarms();
	res.send({ allAlarms });
});

app.listen(port, () => {
	console.log(`[server]: Server is running at http://localhost:${port}`);
});
