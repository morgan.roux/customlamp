import React, { createContext, useContext, useEffect, useState } from "react";
import { lightTheme, darkTheme } from "../config/theme";
import { useMediaQuery } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

interface ThemeContextValue {
	themeToggle: "light" | "dark" | "system";
	setThemeToggle: (theme: "light" | "dark" | "system") => void;
}

export const useMyThemeContext = () => {
	const context = useContext(MyThemeContext);
	if (context === undefined) {
		throw new Error("useMyThemeContext must be used within a MyThemeProvider");
	}
	return context;
};

export const MyThemeContext = createContext<ThemeContextValue | undefined>(undefined);

const MyThemeProvider = ({ children }: { children: React.ReactNode }) => {
	const [themeToggle, setThemeToggle] = useState<"light" | "dark" | "system">("system");
	const darkModeFromSystem = useMediaQuery("(prefers-color-scheme: dark)");
	const prefersDarkMode = themeToggle === "dark" || (themeToggle === "system" && darkModeFromSystem);

	const themeWithLocale = React.useMemo(
		() => createTheme(prefersDarkMode ? darkTheme : lightTheme),
		[prefersDarkMode],
	);
	useEffect(() => {
		const toggle = localStorage.getItem("themeToggle") as "light" | "dark" | "system";
		setThemeToggle(toggle ?? "system");
	}, []);

	const setDarkMode = (theme: "light" | "dark" | "system") => {
		localStorage.setItem("themeToggle", theme);
		setThemeToggle(theme);
	};

	return (
		<MyThemeContext.Provider value={{ themeToggle, setThemeToggle: setDarkMode }}>
			<ThemeProvider theme={themeWithLocale}>{children}</ThemeProvider>
		</MyThemeContext.Provider>
	);
};

export default MyThemeProvider;
