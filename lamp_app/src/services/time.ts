import dayjs from "dayjs";
export { Dayjs } from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";

dayjs.extend(localizedFormat);

export default dayjs;
