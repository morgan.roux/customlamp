import {
	Box,
	Button,
	DialogActions,
	DialogContent,
	DialogTitle,
	IconButton,
	Stack,
	TextField,
	ToggleButton,
	Typography,
} from "@mui/material";
import Dialog from "./Dialog";
import { Close, Delete } from "@mui/icons-material";
import { Alarm, WeekDays, weekDaysName } from "@/types/alarm";
import { useEffect, useState } from "react";
import dayjs from "@/services/time";
import { MultiSectionDigitalClock } from "@mui/x-date-pickers";

interface DialogEditAlarmProps {
	open: boolean;
	onClose: () => void;
	onSave: (alarm: Alarm) => void;
	onDelete: (id: string) => void;
	alarm: Alarm | null;
}

const DialogEditAlarm = ({ open, onClose, onSave, onDelete, alarm }: DialogEditAlarmProps) => {
	const [newAlarm, setNewAlarm] = useState<Alarm | null>(null);
	const isValid =
		!!newAlarm && !!newAlarm.name && newAlarm.name != "" && !!newAlarm.startAt && !!newAlarm.activeDays?.length;
	useEffect(() => {
		setNewAlarm(alarm);
	}, [alarm]);
	return (
		<Dialog open={open} onClose={onClose}>
			{/* <DialogTitle>
				<Stack flexDirection="row" alignItems="center" justifyContent="space-between">
					{!!alarm?.name ? "Edit Alarm" : "New alarm"}
					<IconButton onClick={() => onClose()}>
						<Close />
					</IconButton>
				</Stack>
			</DialogTitle> */}
			<DialogContent>
				<Stack flexDirection="row" alignItems="center">
					<Stack flex={1} />
					<MultiSectionDigitalClock
						sx={{
							flex: 1,
						}}
						ampm={false}
						timeSteps={{ hours: 1, minutes: 1 }}
						value={dayjs()
							.set("hour", parseInt(newAlarm?.startAt?.split(":")[0] ?? dayjs().hour.toString()))
							.set("minute", parseInt(newAlarm?.startAt?.split(":")[1] ?? dayjs().minute.toString()))}
						onChange={(date) => {
							newAlarm && setNewAlarm({ ...newAlarm, startAt: date.format("HH:mm") });
						}}
					/>
					<Stack flex={1} />
				</Stack>
				<Box p={2} />
				<Stack flexDirection="row" alignItems="center" justifyContent="space-between">
					{Object.values(WeekDays).map((day) => (
						<Button
							key={day}
							sx={{ minWidth: 0 }}
							onClick={() => {
								if (newAlarm?.activeDays?.includes(day)) {
									setNewAlarm({
										...newAlarm,
										activeDays: newAlarm?.activeDays?.filter((d) => d !== day),
									});
								} else {
									newAlarm &&
										setNewAlarm({
											...newAlarm,
											activeDays: [...(newAlarm?.activeDays || []), day],
										});
								}
							}}
						>
							<Typography
								variant="caption"
								color={newAlarm?.activeDays?.includes(day) ? "primary" : "InactiveCaptionText"}
							>
								{weekDaysName[day]}
							</Typography>
						</Button>
					))}
				</Stack>
				<Box p={2} />
				<TextField
					label="Name"
					variant="standard"
					fullWidth
					value={newAlarm?.name ?? ""}
					onChange={(e) => newAlarm && setNewAlarm({ ...newAlarm, name: e.target.value })}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={() => onClose()}>Annuler</Button>
				{alarm?.id && (
					<Button
						color="error"
						onClick={() => {
							onDelete(alarm.id!);
							onClose();
						}}
						startIcon={<Delete />}
					>
						Supprimer
					</Button>
				)}
				<Button
					disabled={!isValid}
					onClick={() => {
						!!newAlarm && onSave(newAlarm);
						onClose();
					}}
				>
					Enregistrer
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default DialogEditAlarm;
