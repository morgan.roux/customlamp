import { Dialog, DialogProps, Slide, useMediaQuery, useTheme } from "@mui/material";
import { styled } from "@mui/material/styles";
import { TransitionProps } from "@mui/material/transitions";
import React from "react";

const Transition = React.forwardRef(function Transition(
	props: TransitionProps & {
		children: React.ReactElement<any, any>;
	},
	ref: React.Ref<unknown>,
) {
	return <Slide direction="up" ref={ref} {...props} />;
});

// TODO : find why transition is not working when leave dialog (myabe the way it's handled in the context ? )
const MyDialog = (props: DialogProps) => {
	const { children, ...otherProps } = props;
	const theme = useTheme();
	const matchMobile = !useMediaQuery(theme.breakpoints.up("sm"));
	return (
		<Dialog
			PaperProps={{
				sx: matchMobile
					? {
							minWidth: "100vw",
							position: "absolute",
							bottom: 0,
							marginBottom: 0,
							borderBottomLeftRadius: 0,
							borderBottomRightRadius: 0,
						}
					: {},
			}}
			fullWidth
			TransitionComponent={Transition}
			maxWidth={matchMobile ? "xl" : "md"}
			{...otherProps}
		>
			{props.children}
		</Dialog>
	);
};
export default MyDialog;
