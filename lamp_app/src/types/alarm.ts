export enum WeekDays {
	Sunday = "Sun",
	Monday = "Mon",
	Tuesday = "Tue",
	Wednesday = "Wed",
	Thursday = "Thu",
	Friday = "Fri",
	Saturday = "Sat",
}

export const weekDaysName = {
	[WeekDays.Sunday]: "Dim",
	[WeekDays.Monday]: "Lun",
	[WeekDays.Tuesday]: "Mar",
	[WeekDays.Wednesday]: "Mer",
	[WeekDays.Thursday]: "Jeu",
	[WeekDays.Friday]: "Ven",
	[WeekDays.Saturday]: "Sam",
};

export interface Alarm {
	id?: string;
	name: string;
	startAt: string; //dayjs.format("HH:mm")
	active: boolean;
	activeDays: WeekDays[];
}
