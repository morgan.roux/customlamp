// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { setLight } from "@/services/serverApi";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	if (req.method === "PUT") {
		const { intensity } = req.body;
		await setLight(intensity);
		res.status(200).end();
		return;
	}
	res.status(400).end();
}
