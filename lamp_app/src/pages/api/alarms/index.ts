// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { Alarm } from "@/types/alarm";
import { createAlarm, getAlarms, stopAlarm } from "@/services/serverApi";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	if (req.method === "GET") {
		const ret = await getAlarms();
		return res.status(200).json(ret.data);
		// return res.status(200).json([
		// 	{ id: "1", name: "Alarm 1", startAt: "12:02", active: true, activeDays: ["Sun", "Mon", "Tue"] },
		// 	{ id: "2", name: "Alarm 2", startAt: "12:01", active: true, activeDays: ["Thu"] },
		// ]);
	} else if (req.method === "POST") {
		const data = req.body as Omit<Alarm, "id">;
		const ret = await createAlarm(data);
		return res.status(200).json(ret.data);
	} else if (req.method === "DELETE") {
		const ret = await stopAlarm();
		return res.status(200).json(ret.data);
	}
	res.status(400).end();
}
